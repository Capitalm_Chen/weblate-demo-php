#!/bin/sh

WD=`dirname "$0"`
cd "$WD"
xgettext -L PHP -o locale/messages.pot index.php
sed -i -e 's/CHARSET/UTF-8/' locale/messages.pot
msginit -i locale/messages.pot -l en.UTF-8 --no-translator -o locale/en/LC_MESSAGES/messages.po

